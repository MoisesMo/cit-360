<%--
  Created by IntelliJ IDEA.
  User: moisesemiliocuevamolina
  Date: 22/06/2020
  Time: 11:25
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
  <title>Order Cupcake</title>
  <link rel="stylesheet" href="${pageContext.request.contextPath}/selection.css" />
</head>
<body>
<div class="section">
  <ul class="grid">
    <li>
      <div class="box img1">
        <a href="cupcake.jsp">
          <div class="info">
            <h1 class="center">Order your CupCakes</h1>
          </div></a>
      </div>
    </li>
  </ul>
</div>
</body>
</html>