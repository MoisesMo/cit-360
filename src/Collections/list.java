package Collections;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Scanner;

public class list {

    public static void main(String[] args) {

        List<String> CampList = new LinkedList<String>();
        Scanner in = new Scanner(System.in);
        System.out.println(initial());

        String newItem = in.nextLine();
        while (newItem.equalsIgnoreCase("Finish List")==false) {

            if (newItem.equalsIgnoreCase("Show List")){

                if (CampList.isEmpty()) {
                    System.out.println("Your Collections.list is empty.");
                    System.out.println(initial());
                    newItem = in.nextLine();
                } else {
                    System.out.println("Items of your Collections.list:");

                    List<String> ArrayList = new ArrayList<String>(CampList);
                    Iterator<String> iterator = ArrayList.iterator();
                    if (iterator.hasNext()) {
                        do {
                            System.out.println(iterator.next());
                        } while (iterator.hasNext());
                    }
                    newItem = in.nextLine();
                }
            } else {
                CampList.add(newItem);
                System.out.println(initial());
                newItem = in.nextLine();
            }
        }
        System.out.println("Have a nice Camp." + "\r\n" + "Your Collections.list:" + CampList);
        in.close();
    }

    public static String initial() {
        return "\n Add items for your Camp.\n Options: Show List, Finish List \n Write your Camp Collections.list: \n";
    }

}