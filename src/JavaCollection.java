import java.util.*;


public class JavaCollection {
    public static void main(String[] args) {
        System.out.println("List" + "\r\n" + "ArrayList");
        //ArrayList
        ArrayList<String> Fruit = new ArrayList<String>();
        Fruit.add("Apple");
        Fruit.add("Blueberry");
        Fruit.add("Cherry");
        Fruit.add("Grape");
        Fruit.add("Mango");

        Fruit.remove("Grape");
        for(String s: Fruit) {
            System.out.println(s);
        }
//LinkedList
        System.out.println("\r\n" + "LinkedList");
        LinkedList<String> names = new LinkedList<String>();
        names.add("Anna");
        names.add("Carlo");
        names.add("Anabel");
        names.add("Emilio");

        names.remove("Emilio");

        for(String s: names) {
            System.out.println(s);
        }
//HashMap
        System.out.println("\r\n" + "HashMap");
        HashMap<String, Integer> ages = new HashMap<String, Integer>();
        ages.put("Moises",26 );
        ages.put("Realy", 52);
        ages.put("Perld", 133);
        System.out.println(ages.get("Realy"));
//HashSet
        System.out.println("\r\n" + "HashSet");
        HashSet<String> triss = new HashSet<String>();
        triss.add("AAA");
        triss.add("DDD");
        triss.add("CCC");
        triss.add("zzz");
        triss.add("BBB");
        System.out.println(triss);

// Collectio.Sort(String)
        System.out.println("\r\n" + "Lastname Collection");
        ArrayList<String> Lastnames = new ArrayList<String>();
        Lastnames.add("Cueva");
        Lastnames.add("Moly");
        Lastnames.add("Andrades");
        Lastnames.add("Williams");
        Lastnames.add("Jackson");
        Collections.sort(Lastnames);

        System.out.println(Lastnames);

// Collectio-Sort(integer)
        System.out.println("\r\n" + "List With Integer");
        ArrayList<Integer> nums = new ArrayList<Integer>();
        nums.add(100);
        nums.add(3);
        nums.add(11);
        nums.add(15);
        nums.add(11);

        Collections.sort(nums);
        System.out.println(nums);

        // Collectio-Sort(integer)
        System.out.println("\r\n" + "List With Integer");
        ArrayList<Integer> num = new ArrayList<Integer>();
        num.add(100);
        num.add(3);
        num.add(11);
        num.add(15);
        num.add(11);

        Iterator<Integer> seer = num.iterator();
        while(seer.hasNext()){
            int i = seer.next();
            System.out.println(i);
        }

// Collectio-Sort(string-Iterator)

        LinkedList<String> Citys = new LinkedList<String>();
        Citys.add("VR");
        Citys.add("ID");
        Citys.add("Provo");
        Citys.add("NY");

        Iterator CI = Citys.iterator();
        // Iterating the list in forward direction
        System.out.println("\r\n" + "LinkedList elements:");
        while(CI.hasNext()){
            System.out.println(CI.next());
        }
    }
}
