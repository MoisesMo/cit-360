package dao;

import org.hibernate.Session;
import org.hibernate.Transaction;
import model.User;
import util.HibernateUtil;


public class UserDao {
// this help use to start and commit transaction
    public void saveUser(User user) {
        Transaction transaction = null;
        try (Session session = HibernateUtil.getSessionFactory().openSession()) {
            transaction = session.beginTransaction();
            session.save(user);
            transaction.commit();

        } catch (Exception e) {
            if (transaction != null) {
                transaction.rollback();
            }
            e.printStackTrace();
        }
    }
}