<%--
  Created by IntelliJ IDEA.
  User: moisesemiliocuevamolina
  Date: 17/07/2020
  Time: 00:55
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
         pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
    <meta charset="ISO-8859-1">
    <title>registration</title>

</head>

</head>
<body>
<div class="container">
    <div style="color: tomato;">
        <h2>User Registration </h2>
    </div>
    <hr>
    <div>

        <div>

            <h2>User Register Form</h2>
            <div>

                <form action="<%=request.getContextPath()%>/register" method="post">

                    <div>
                        <label for="firstName">First Name:</label> <input type="text"
                                                                      class="form-control" id="firstName" placeholder="First Name"
                                                                      name="firstName" required>
                    </div>

                    <div>
                        <label for="middleName">Middle Name:</label> <input type="text"
                                                                     class="form-control" id="middleName" placeholder="Middle Name"
                                                                     name="middleName" required>
                    </div>

                    <div>
                        <label for="lastName">Last Name:</label> <input type="text"
                                                                            class="form-control" id="lastName" placeholder="Last Name"
                                                                            name="lastName" required>
                    </div>

                    <div>
                        <label for="birthday">Birthday Date:</label> <input type="text"
                                                                     class="form-control" id="birthday" placeholder="Birthday Date"
                                                                     name="birthday" required>
                    </div>

                    <div>
                        <label for="mobile">Mobile Phone:</label> <input type="text"
                                                                     class="form-control" id="mobile" placeholder="Mobile Phone"
                                                                     name="mobile" required>
                    </div>

                    <div>
                        <label for="username">User Name:</label> <input type="text"
                                                                     class="form-control" id="username" placeholder="User Name"
                                                                     name="username" required>
                    </div>

                    <div>
                        <label for="password">Password:</label> <input type="password"
                                                                    class="form-control" id="password" placeholder="Password"
                                                                    name="password" required>
                    </div>

                    <button type="submit" class="btn btn-primary">Submit</button>

                </form>
            </div>
        </div>
    </div>
</div>
</body>
</html>