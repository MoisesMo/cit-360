package JUnit;

import org.junit.Rule;
import org.junit.jupiter.api.Test;
import org.junit.rules.ExpectedException;

import java.io.FileReader;
import java.io.IOException;

import static org.hamcrest.core.StringStartsWith.startsWith;
import static org.junit.Assert.*;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

class JUcalculationTest {

    @Test
   public void resultPass130equalsresult() {
        JUcalculation test = new JUcalculation();

        int result = test.math(10, 200, 150, 120);

       assertEquals(130, result);


    }
        @Test
        public void resultPass50equalsresult() {
            JUcalculation test = new JUcalculation();

            int result = test.math(10, 20, 10, 20);

            assertTrue (50 <= result);

    }

    @Test
    public void resultPassUp200OrEqual() {
        JUcalculation test = new JUcalculation();

        int result = test.math(100, 220, 120, 320);

        assertFalse(200 >= result);

    }


    @Test
    public void resultPass() {
        JUcalculation test = new JUcalculation();

        int result = test.math(100, 0, 5, 2);

        assertNotNull(result);

    }


    @Test
    public void resultPass0equalsresult() {
        JUcalculation test = new JUcalculation();

        int result = test.math(0, 20, 1000, 220);

        assertSame(0,result);

    }

    @Test
    public void resultPass502equalsresult() {
        JUcalculation test = new JUcalculation();

        int result = test.math(30, 230, 120, 210);

        assertNotSame(1205,result);

    }


    //Testing with ExpectedException Rule.
    @Rule
    public ExpectedException thrown = ExpectedException.none();

    @Test
    public void testReadFile3() throws IOException {

        thrown.expect(IOException.class);
        thrown.expectMessage(startsWith("inf.json - No such file or directory"));
        FileReader reader = new FileReader("inf.json");
        reader.read();
        reader.close();
        
    }
}

