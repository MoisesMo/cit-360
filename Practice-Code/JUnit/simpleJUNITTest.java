package JUnit;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class simpleJUNITTest {
    simpleJUNIT calculation = new simpleJUNIT();
    int sum = calculation.sum(22, 545);
    int testSum = 567;
    @Test
    void testSum() {
        System.out.println("@Test sum(): " + sum + " = " + testSum);
        assertEquals(sum, testSum);
        
    }
}