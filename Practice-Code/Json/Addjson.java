package Json;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.LinkedHashMap;
import java.util.Map;


public class Addjson {

        public static void main(String[] args) throws FileNotFoundException {
            JSONObject info = new JSONObject();
            // add data to the Json Object
            info.put("firstName", "Moises E");
            info.put("lastName", "Cueva");
            info.put("birthday", "11.02.1993");

            // Add the info with linkedhashmap
            Map mapping = new LinkedHashMap(2);
            mapping.put("streetAddress", " 1009 Ruskin Junction");
            mapping.put("city", "Verona");
            // add JSONObject
            info.put("address", mapping);
            // Section for the phoneNumbers
            JSONArray phoneNumbers = new JSONArray();

            mapping = new LinkedHashMap(2);
            mapping.put("type", "home");
            mapping.put("number", "361-618-8741");
            //Mapping the phone
            phoneNumbers.add(mapping);

            mapping = new LinkedHashMap(2);
            mapping.put("type", "CellPhone");
            mapping.put("number", "361-618-8742");

            // add the map in list
            phoneNumbers.add(mapping);

            // add the number to the Json Object
            info.put("phoneNumbers", phoneNumbers);

            // add info in data.json
            PrintWriter pw = new PrintWriter("data.json");
            pw.write(info.toJSONString());

            pw.flush();
            pw.close();
        }
    }

