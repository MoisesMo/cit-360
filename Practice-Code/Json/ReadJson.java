package Json;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import java.io.FileReader;
import java.util.Iterator;
import java.util.Map;

public class ReadJson {
    public static void main(String[] args) throws Exception {
       //get the file - data.json
        Object obj = new JSONParser().parse(new FileReader("data.json"));

        // take obj
        JSONObject info = (JSONObject) obj;

        // read the firstName and the lastName
        String firstname = (String) info.get("firstName");
        String lastname = (String) info.get("lastName");

        // show to display the name and the lastname
        System.out.println("Name: " + firstname);
        System.out.println("Last name: " + lastname);

        // read Birthday
        String birthday = (String) info.get("birthday");
        System.out.println("Birthday: " + birthday);

        // find the address
        Map address = ((Map)info.get("address"));

        //  address Map
        Iterator<Map.Entry> itr1 = address.entrySet().iterator();
        while (itr1.hasNext()) {
            Map.Entry pair = itr1.next();
            System.out.println(pair.getKey() + " : " + pair.getValue());
        }

        // read phoneNumbers
        JSONArray phoneNumbers = (JSONArray) info.get("phoneNumbers");

        // iterating with phoneNumbers
        Iterator itr2 = phoneNumbers.iterator();

        while (itr2.hasNext()) {
            itr1 = ((Map) itr2.next()).entrySet().iterator();
            while (itr1.hasNext()) {
                Map.Entry pair = itr1.next();
                System.out.println(pair.getKey() + " : " + pair.getValue());
            }
        }
    }
}


