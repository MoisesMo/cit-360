package TRE;

import java.io.PrintWriter;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

public class Test {

    public static void main(String[] args) {
        PrintWriter print1 = new PrintWriter(System.out);
        // we start to run the code
        print1.write("- Done - Start "+"\r\n");
        print1.flush();

        // here we call the Object3=Thread
        Object3 T5 = new Object3();
        T5.start();

        // here we call the Object4=Runnable
        Thread T6 = new Thread(new Object4());
        T6.start();

        // in 1 second it follow sleep
        try {
            Thread.sleep(1000);
            print1.write("- Done - 1 second sleep"+"\r\n");

            // Exeception for anny mistake in the follow sleep
        } catch (InterruptedException e) {
            print1.write("There is a problem check your sleep" + e + "\r\n");
        }
        // Execute
        ExecutorService Executor = Executors.newSingleThreadScheduledExecutor();
        Executor.submit(() -> {
            // get the name of the Pool
            String Name = Thread.currentThread().getName();
            print1.write("- Done - Pool: " + Name + "\r\n" );
            // Get the Status of the pool
            Thread.State Pool = Thread.currentThread().getState();
            print1.write("- Done - Pool is " + Pool + "\r\n" ); });

        try {
            Executor.shutdown();
            Executor.awaitTermination(1, TimeUnit.SECONDS);
            print1.write("- Done - shutdown the Executor" + "\r\n");
        }
        catch (InterruptedException e) {
           print1.write("Executor can't shutdown");

        }
        finally {
            if (!Executor.isTerminated()) {
                print1.write("Executor shutdown try one more time"+ "\r\n");
            }

            Executor.shutdownNow();
            print1.write("- Done - Correct shutdown the Executor" + "\r\n");
            print1.flush();
        }

        print1.close();
}

}

