package TRE;

import java.io.PrintWriter;

public class Test1 {
    public static void main(String args[])
    {// create two object for the text
        Object1 T1 = new Object1("Object1 1: "),T2 = new Object1("Object1 2: ") ;
        T1.start();
        T2.start();
        //Pass the theards/ object
        boolean T1Pass = true, T2Pass = true;
        //Finish the Theards/ time of the object
        PrintWriter print = new PrintWriter(System.out);
        do {
            if (T1Pass && !T1.isAlive()) {

                T1Pass = false;
                print.write("Object1 1 finish."+ "\r\n");
                print.flush();
            }
            if (T2Pass && !T2.isAlive()) {
                T2Pass = false;
                print.write("Object1 2 finish."+ "\r\n");
                print.flush();
            }

        } while(T1Pass || T2Pass);
        print.close();
    }
}
