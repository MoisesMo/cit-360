package TRE;

import java.io.PrintWriter;

public class Test2 {
    public static void main(String args[])
    {
        Object2 T3 = new Object2("Object2 3: "), T4 = new Object2("Object2 4: ");
        T3.start();
        T4.start();
        //Pass the theards/ object
        boolean T3Pass = true, T4Pass = true;
        PrintWriter print2 = new PrintWriter(System.out);
        do {
            if (T3Pass && !T3.isAlive()) {
                T3Pass = false;
                print2.write("Object2 3 finish.."+ "\r\n"+"\r\n");
            }
            if (T4Pass && !T4.isAlive()) {
                T4Pass = false;
                print2.write("Object2 4 finish.."+ "\r\n");
            }
            print2.flush();
        } while(T3Pass || T4Pass);
        print2.close();
    }
}

