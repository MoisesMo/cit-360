package TRE;

import java.io.PrintWriter;

class Object1 extends java.lang.Thread
{
    PrintWriter print0 = new PrintWriter(System.out);
    //The text- Object1 that will be show
    static String text[] =
            { "Today", "We", "Learn","About", "Threads","Runnables", " Executors."};
// identify the Object1
    public Object1(String id)
    {
        super(id);
    }
// rum the Object1 with different secuence
    public void run()
    {
        String name = getName();
        for (int i=1;i<text.length;++i) {
            Wait();
            print0.write(name + text[i]+ "\r\n");
            print0.flush();
        }
    }
// Waits for the threads/Object1 to finish- With random time
    void Wait()
    {
        try {
            // wait 500
            sleep((long)(500*Math.random()));
            // something happen with the execution
        } catch (InterruptedException x) {
            print0.write("Problem!-Stop it"+ "\r\n");
            print0.flush();
        }

    }
}
