package TRE;

import java.io.PrintWriter;

public class Object2 extends Thread
{
    //The text- Object1 that will be show by the syncrony
    static String message[] =
            { "Today", "We", "Learn","About", "Threads","Runnables", "Executors."};
    // identify the Object1
    public Object2(String id)
    {
        super(id);
    }
    // rum the Object1 with different secuence
    public void run()
    {
        try {
            Sequence.displayList(getName(),message);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    // Waits for the threads/Object1 to finish- With random time
    void Wait()
    {
        PrintWriter print02 = new PrintWriter(System.out);
        try {
            // wait 500
            sleep((long)(500*Math.random()));
            // something happen with the execution
        } catch (InterruptedException x) {
            print02.write("Problem!-Stop it"+ "\r\n");
        }
        print02.flush();
    }
}

