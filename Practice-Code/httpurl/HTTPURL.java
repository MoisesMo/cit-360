package httpurl;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;


public class HTTPURL {

        public static void main (String[] args) {

            String output = getUrlContents("https://moisescuevam.github.io/Hello-word/");

            System.out.println(output);
        }

        private static String getUrlContents(String theUrl) {

            StringBuilder content = new StringBuilder();

            try {
                URL url = new URL(theUrl);
                URLConnection urlConnection = url.openConnection();

                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(urlConnection.getInputStream()));

                String line;

                while((line = bufferedReader.readLine()) != null) {
                    content.append(line + "\n");
                }
                bufferedReader.close();
            } catch (MalformedURLException e){

            return "problem loging";

            }
            catch (Exception e){
                return "fix url " + e.getLocalizedMessage();
            }
            return content.toString();
        }
    }