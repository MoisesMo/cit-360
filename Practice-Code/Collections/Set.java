package Collections;


import java.util.HashSet;

import java.util.TreeSet;

public class Set {
public static void main(String args[]) {
        // The int = List of years in the company
        int YearList[] = {10, 3, 2, 31, 15, 27};
        java.util.Set<Integer> AgesExperience = new HashSet<Integer>();
        // Sort the list
        try {  for (int i = 0; i < 6; i++) { AgesExperience.add(YearList[i]); }
        //keep the elements sorted
        TreeSet SelectSet = new TreeSet<Integer>(AgesExperience);
        // Display the List by keeping the sort
        System.out.println("The Age List is:");
        System.out.println(SelectSet);
        // Select the first = the Youngest in the company
        System.out.println("The youngest in the Company is: "
        + (Integer) SelectSet.first());
        // Select the last = the Oldest in the company
        System.out.println("The oldest in the Company is: "
        + (Integer) SelectSet.last()); }
        catch (Exception a) {
                System.out.println("Invalid data");
        }
}
}
