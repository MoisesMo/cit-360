package JacksonJson;

import java.util.ArrayList;

public class TestingJson {

    public static void main(String[] args){
        Customer c1 = new Customer(01, "Baudoin", "Mustang", 25, "Verona");
        Customer c2 = new Customer(02, "Bail", "Camero", 29, "Milano" );
        Customer c3 = new Customer(03, "Nichole", "Probe", 18, "Verona" );
        Customer c4 = new Customer(04, "Gregg", "Focus", 35, "Milano" );
        SuperCustomer s1 = new SuperCustomer(05, "Emilio", "Molina", 39, "Verona", 109, true);
        SuperCustomer s2 = new SuperCustomer(06, "Rahel", "Cueva", 39, "Verona", 200, true);

        CustomerList cl = new CustomerList();

        ArrayList<Customer> someList = new ArrayList<Customer>();

        someList.add(c1);
        someList.add(c2);
        someList.add(c3);
        someList.add(c4);
        someList.add(s1);
        someList.add(s2);


        cl.setCustomerList (someList);

        DataManagement dm = new DataManagement();
        dm.writeAllData(cl);


        CustomerList cl2 = new CustomerList();
        cl2 = dm.readAllData();

        System.out.println("The Customer list from the Json file is :"
                        +  "\r\n"+ cl2.getCustomerList().get(0)
                        + "\r\n" + cl2.getCustomerList().get(1)
                        + "\r\n" + cl2.getCustomerList().get(2)
                        + "\r\n" + cl2.getCustomerList().get(3)
        );

        System.out.println("The Super Customer is: " + "\r\n"+ cl2.getCustomerList().get(4)
                + "\r\n"+ cl2.getCustomerList().get(5)) ;

    }
}
