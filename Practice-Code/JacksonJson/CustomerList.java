package JacksonJson;

import java.util.ArrayList;
import java.util.List;

public class CustomerList {
    private List<Customer> customerList = new ArrayList<Customer>();

    public CustomerList() {
        this.customerList = new ArrayList<Customer>();
    }

    public CustomerList(List<Customer> customerList) {
        super();
        this.customerList = customerList;
    }

    public List<Customer> getCustomerList() {
        return customerList;
    }

    public void setCustomerList(List<Customer> customerList) {
        this.customerList = customerList;
    }


}

