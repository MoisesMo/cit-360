package JacksonJson;

import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;

@JsonTypeInfo(
        use = JsonTypeInfo.Id.NAME,
        include = JsonTypeInfo.As.PROPERTY,
        property = "type")
@JsonSubTypes({
        @JsonSubTypes.Type(value = Customer.class, name = "customer"),
        @JsonSubTypes.Type(value = SuperCustomer.class, name = "super")
})

public class Customer {
    private int id;
    private String first_name;
    private String last_name;
    private int age;
    private String city;

    public Customer()  {

    }

    public Customer(int id, String first_name, String last_name, int age, String city) {
        super();
        this.id = id;
        this.first_name = first_name;
        this.last_name = last_name;
        this.age = age;
        this.city = city;

    }

    @Override
    public String toString() {
        return "" +
                "  id =" + id +
                ", first_name ='" + first_name + '\'' +
                ", last_name ='" + last_name + '\'' +
                ", age =" + age +
                ", city ="+ city +
                ' ';
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getFirst_name() {
        return first_name;
    }

    public void setFirst_name(String first_name) {
        this.first_name = first_name;
    }

    public String getLast_name() {
        return last_name;
    }

    public void setLast_name(String last_name) {
        this.last_name = last_name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }
}

