package JacksonJson;

public class SuperCustomer extends Customer {

    private int totalCost;
    private Boolean trust;

    public SuperCustomer() {
        super();
    }

    public SuperCustomer(int id, String first_name, String last_name, int age, String city, int totalCost, Boolean trust) {
        super(id, first_name, last_name, age, city);
        this.totalCost = totalCost;
        this.trust = trust;
    }

    public int getTotalCost() {
        return totalCost;
    }

    public void setTotalCost(int totalCost) {
        this.totalCost = totalCost;
    }

    public Boolean getTrust() {
        return trust;
    }

    public void setTrust(Boolean trust) {
        this.trust = trust;
    }

    @Override
    public String toString() {
        return " " +
                " id =" + super.getId() +
                ", first_name ='" + super.getFirst_name() + '\'' +
                ", last_name ='" + super.getLast_name() + '\'' +
                ", age =" + super.getAge() +
                ", city ="+ super.getCity() +
                ", totalCost =" + totalCost +
                ", trust =" + trust +
                ' ';
    }
}
