package JacksonJson;

import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.File;
import java.io.IOException;

public class DataManagement {

    public CustomerList readAllData() {
      CustomerList cl = new CustomerList();
      try {
          cl = new ObjectMapper().readerFor(CustomerList.class).readValue(new File("customer.json"));
        }catch (IOException e){
            e.printStackTrace();
    }
        return cl;
    }


    public void writeAllData(CustomerList theList){
        ObjectMapper om = new ObjectMapper();
        try {
            om.writerWithDefaultPrettyPrinter().writeValue(new File("customer.json"), theList);
        } catch (JsonGenerationException e){
            e.printStackTrace();
        } catch (JsonMappingException e){
            e.printStackTrace();
        }catch (IOException e){
            e.printStackTrace();
        }


    }


}

